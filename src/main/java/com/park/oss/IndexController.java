package com.park.oss;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.park.oss.model.SysUser;

@Controller
public class IndexController {
	 @RequestMapping(value = "/console", method = RequestMethod.GET)
	 public ModelAndView park() {
		ModelAndView view = new ModelAndView("redirect:/park/index.html");
        return view;
	 }
	 
	 @RequestMapping(value = "/mobile", method = RequestMethod.GET)
	 public ModelAndView mobile() {
		ModelAndView view = new ModelAndView("/mobile/login.btl");
        return view;
	 }
	 
	 @RequestMapping(value = "/test", method = RequestMethod.GET)
	 public ModelAndView test() {
		ModelAndView view = new ModelAndView("/test.jsp");
        return view;
	 }
}
