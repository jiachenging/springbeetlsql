search
===

	select #page()# from park

searchFree
===

* 搜索可用得停车场,3000米以内

	select pk.*,
	(select count(1) from place_rent re where re.park_id = pk.id and status=0
	and day = #day# ) freeCount,
	(select c.status from car_place c where c.park_id=pk.id and c.status in (0,1,2) 
    		and user_id = #userId# ) carStatus,
    	get_distance(pk.lon,pk.lat,#lon#,#lat#) distance
	from park pk  where   get_distance(pk.lon,pk.lat,#lon#,#lat#) <3000

	
sum
===
* 注释

	select s.*,p.name park_name from (
		select park_id,sum(park_money)  park_money,sum(platform_money) platform_money ,sum(owner_money) 		owner_money ,min(create_time) min_create_time,
		max(create_time) max_create_time
	from payment  
	where 1=1 
	@if(!isEmpty(parId)){
	and park_id=#parkId#
	@}
	
	group by park_id ) s left join park p on s.park_id=p.id order by s.park_id desc 

